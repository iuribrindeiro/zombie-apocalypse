## APRESENTAÇÃO DO PROJETO

Utilizando do padrão de arquitetura DDD (Domain Driver Design), desenvolvi a Api para atender as regras de negocio solicitadas pelo teste.

## Principais Frameworks Utilizados:

- Identity Framework 2.2.1
- Entity Framework 6.1.3
- Microsoft WebApi
- Simple Injector

## Padrões de Projeto Utilizados:

- IoC Injeção de Dependencia
- Repository Pattern
- Service Pattern

## Front-End Framework
- React (Single Page Application)

## Instalação

####1 
 Baixe o projeto do repositório: git clone https://iuribrindeiro@bitbucket.org/iuribrindeiro/zombie-apocalypse.git

####2
Abra o projeto com o visual studio

####3
 Altere a connection string para se conectar com o arquivo de banco de dados local dentro de src/ZombieAPI.Infra.Data/zombies-test.mdf. Você vai precisar colocar o seu caminho relativo dentro da connection string no parametro "AttachDbFilename"

####4
Abra o nuget package console e selecione o projeto padrão do console como: ZombieAPI.Infra.CrossCutting.Identity e rode o comando Update-Database
(já que aderí ao DDD, separei as camadas da aplicação, no entanto, não foi possível desacoplar o Context do Identity, por isso a necessidade desse procedimento)

####5
Agora selecione o projeto padrão como ZombieAPI.Infra.Data e rode novamente o comando Update-Database

####6
Agora, na aba do Solution Explorer, abra a pasta 0 - Presentation e selecione o projeto ZombieAPI.Presentation.API como projeto de inicialização (clique com direito, e "Definir como projeto de inicialização")

####7
Coloque a aplicação para rodar clicando no "Play" do Visual Studio e aguarde a inicialização terminar.

####8
No projeto de front-end que consome a Api, será necessário baixar o nodejs para rodar o npm
(caso ainda não tenha) Link do download: https://nodejs.org/en/

####8
Após efetuado o download, vá na pasta src/fron-end/zombie-react e digite o comando npm install

####9
Quando a instalação dos modulus terminar, rode o comando npm start

####10
Caso a porta em que o projeto front-end esteja rodando n seja a 3000, será necessário mudar uma configuração de Cors na Api:
dentro de ZombieAPI.Presentation.API/Web.congi altere a linha 104 para 
<add name="Access-Control-Allow-Origin" value="http://localhost:SUAPORTA" />

####11
Abra o navegador e crie um usuário novo com as permissões de "Membro" ou utilize um admin já cadastrado no banco.
Login: admin@zombies.com Senha: Admin123#


## Aplicação

- Permitir todos os usuário de excluir um recurso ou visualizar as movimentações de recursos (entrada e saída) não me pareceu o certo. Somente um administrador pode executar essas ações. Ou seja, para visualizar as movimentações, você vai precisar logar com o usuário das credenciais acima, já que o mesmo possui permissões de "Admin";

OBS: Caso ocorra algum erro durante a instalação, favor entrar em contato.
