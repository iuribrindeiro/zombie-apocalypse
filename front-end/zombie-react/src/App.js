import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LoginSection from './Sections/Login';
import Home from './Sections/Home';
import RecursoSection from './Sections/Recurso';
import Cadastro from './Sections/Cadastro';

class App extends Component {
  render() {
    return (
      <Router>        
          <Switch>
            <Route exact path="/novo-recurso" component={RecursoSection}/>
            <Route path="/editar-recurso/:id" component={RecursoSection} />
            <Route path="/login/:message?" component={LoginSection} />
            <Route path="/cadastro" component={Cadastro}/>
            <Route path="/:message?" component={Home} />
          </Switch>        
      </Router>
    );
  }
}

export default App;
