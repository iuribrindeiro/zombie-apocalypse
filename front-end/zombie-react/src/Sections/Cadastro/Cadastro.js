import React, { Component } from 'react';
import UsuarioManager from '../../Services/Manager/UsuarioManager';
import { Redirect, Link } from 'react-router-dom';
import { FormGroup, FormControl, ControlLabel, Button, Alert, Form, Col } from 'react-bootstrap';

export default class Cadastro extends Component {
    constructor(props) {
        super(props);
        this.usuarioManager = new UsuarioManager();
        this.state = {
            sucesso: false,
            error: false,
            nome: false,
            email: false,
            emailConfirm: false, 
            password: false,
            passwordConfirm: false
        };
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        if (this.fieldsAreValid()) {
            let data = {
                nome: this.state.nome,
                email: this.state.email,
                password: this.state.password
            };
            this.usuarioManager.cadastrar(data, (response) => {
                this.setState({
                    sucesso: true
                });
            }, (error) => {
                this.setState({
                    error: true
                });
            });
        }
    }

    fieldsAreValid = () => {
        return this.state.nome.length >= 5 && this.state.email.length >= 10 &&
            this.state.email === this.state.emailConfirm && this.passwordIsValid()
    }

    passwordIsValid = () => {
        let password = this.state.password;
        let regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        return regex.test(password) &&
            this.state.passwordConfirm === this.state.password
    }

    handleInputChange = (e) => {
        let name = e.target.name;
        this.setState({ [name]: e.target.value });
    }

    getValidationStateConfirmEmail = () => {
        if (this.state.emailConfirm === false) return '';

        const isValid = this.state.emailConfirm === this.state.email;
        return isValid ? '' : 'error';
    }

    getValidationStateConfirmSenha = () => {
        if (this.state.passwordConfirm === false) return '';

        const isValid = this.state.passwordConfirm === this.state.password;
        return isValid ? '' : 'error';
    }

    getValidationStateEmail = () => {
        if (this.state.email === false) return '';

        let email = this.state.email;
        let regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValid = regex.test(email);
        return isValid ? '' : 'error';
    }

    getValidationStateNome = () => {
        if (this.state.nome === false) return '';

        const isValid = this.state.nome.length >= 5;
        return isValid ? '' : 'error';
    }

    getValidationStateSenha = () => {
        if (this.state.password === false) return '';

        let password = this.state.password;
        let regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        const isValid = regex.test(password);
        return isValid ? '' : 'error';
    }

    render() {
        return(

            <section className="container col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                <div className="row col-sm-12 centered center-vertical">   
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h1>Cadastrar</h1>
                        </div>
                        <header>
                            {(this.state.sucesso || this.state.error) &&
                                <Alert bsStyle={this.state.sucesso ? 'success' : 'danger'}>
                                    {this.state.sucesso === true ? (
                                        <div>
                                            <strong>Holy guacamole!</strong> 
                                            <span>Cadastro realizado com sucesso! Clique <Link to="/login">aqui</Link> para logar</span>
                                        </div>
                                    ) : (
                                        <div>
                                            <strong>Holy guacamole!</strong> 
                                            <span>Erro no servidor</span>
                                        </div>
                                    )}
                                </Alert>
                            }
                        </header>
                        <div className="panel-body">
                            <Form onSubmit={this.handleFormSubmit.bind(this)}>
                                <FormGroup className="row" controlId="nome" validationState={this.getValidationStateNome()}>
                                    <Col sm={12}>
                                        <ControlLabel>Nome *</ControlLabel>
                                        <FormControl 
                                            onChange={this.handleInputChange.bind(this)} 
                                            type="text" 
                                            placeholder="Nome"
                                            name="nome"
                                            value={this.state.nome ? this.state.nome : ''} />
                                    </Col>
                                </FormGroup>
                                <FormGroup className="row" controlId="email" validationState={this.getValidationStateEmail()}>
                                    <Col sm={12}>
                                        <ControlLabel>Email *</ControlLabel>
                                        <FormControl 
                                            name="email" 
                                            type="email" 
                                            placeholder="Email"
                                            onChange={this.handleInputChange.bind(this)} 
                                            value={this.state.email ? this.state.email : ''} />
                                    </Col>
                                </FormGroup>
                                <FormGroup className="row" controlId="email" validationState={this.getValidationStateConfirmEmail()}>
                                    <Col sm={12}>
                                        <ControlLabel>Confirmação Email *</ControlLabel>
                                        <FormControl 
                                            name="emailConfirm"
                                            type="email"
                                            placeholder="Confirme o Email"
                                            onChange={this.handleInputChange.bind(this)} 
                                            value={this.state.emailConfirm ? this.state.emailConfirm : ''} />
                                    </Col>
                                </FormGroup>
                                <FormGroup className="row" controlId="password" validationState={this.getValidationStateSenha()}>
                                    <Col sm={12}>
                                        <ControlLabel>Senha(A senha deve conter entre 6 e 16 caracteres, sendo pelo menos 1 caractere maiúsculo, 1 especial, 1 número) *</ControlLabel>
                                        <FormControl 
                                            name="password"
                                            type="password"
                                            placeholder="Senha"
                                            onChange={this.handleInputChange.bind(this)} 
                                            value={this.state.password ? this.state.password : ''} />
                                    </Col>
                                </FormGroup>
                                <FormGroup className="row" controlId="passwordConfirm" validationState={this.getValidationStateConfirmSenha()}>
                                    <Col sm={12}>
                                        <ControlLabel>Confirmação Senha *</ControlLabel>
                                        <FormControl 
                                            name="passwordConfirm"
                                            type="password"
                                            placeholder="Confirme a Senha"
                                            onChange={this.handleInputChange.bind(this)} 
                                            value={this.state.passwordConfirm ? this.state.passwordConfirm : ''} />
                                    </Col>
                                </FormGroup>
                                <FormGroup className="row">
                                    <Col sm={12}>
                                        <Button type="submit" bsStyle="success">Confirmar</Button>
                                        <Link style={{marginLeft: '5%'}} className="btn btn-danger" to="/login">Cancelar</Link>
                                    </Col>
                                </FormGroup>
                            </Form>      
                        </div>
                    </div>             
                </div>
            </section>
        );
    }
}