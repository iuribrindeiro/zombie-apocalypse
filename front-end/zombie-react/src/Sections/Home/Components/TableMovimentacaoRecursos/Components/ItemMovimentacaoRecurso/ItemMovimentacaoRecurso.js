import React, { Component } from 'react';

export default class ItemMovimentacaoRecurso extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <tr>                        
                <td>{this.props.movimentacaoRecurso.usuario.userName}</td>
                <td>{this.props.acao + this.props.movimentacaoRecurso.quantidade}</td>
                <td>{this.props.movimentacaoRecurso.recurso.descricao}</td>
                <td>{this.props.movimentacaoRecurso.dataAtualizacao}</td>
            </tr>  
        );
    }
}