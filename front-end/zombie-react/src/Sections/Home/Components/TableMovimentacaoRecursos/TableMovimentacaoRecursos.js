import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import RecursosManager from '../../../../Services/Manager/RecursosManager';
import ItemMovimentacaoRecurso from './Components/ItemMovimentacaoRecurso';

export default class TableMovimentacaoRecursos extends Component {
    constructor(props) {
        super(props);
        this.recursosManager = new RecursosManager();
        this.state = {
            buscandoRecursos: false
        };
    }

    componentWillMount = () => {
        this.setState({buscandoRecursos: true});
        this.buscarMovimentacaoRecursos();
    }

    buscarMovimentacaoRecursos = () => {
        this.recursosManager.findMovimentacoesRecursos((movimentacoesRecursos) => {
            this.props.parent.setState({movimentacaoRecursos: movimentacoesRecursos});
            this.setState({buscandoRecursos: false});
        }, (loginErrorMessage) => {
            this.props.parent.setState({loginErrorMessage: loginErrorMessage});
            this.setState({buscandoRecursos: false});
        }, (error) => {
            this.props.parent.setState({error: error.message}); 
            this.setState({buscandoRecursos: false});
        });
    }

    render() {
        
            if (this.state.buscandoRecursos) {
                return(
                    <div className="loader col-sm-offset-5"></div>
                );
            }

            const movimentacaoRecursos = this.props.movimentacaoRecursos.map((movimentacaoRecurso) => 
                <ItemMovimentacaoRecurso 
                    acao={movimentacaoRecurso.acao == 1 ? 'Adicionou ' : movimentacaoRecurso.acao == 2 ? 'Criou com ' : 'Retirou '} 
                    movimentacaoRecurso={movimentacaoRecurso}
                    key={movimentacaoRecurso.id} />
            );
            
            return(
                <Table responsive>
                    <thead>
                        <tr>
                            <th>Usuário</th>
                            <th>Ação</th>
                            <th>Recurso</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        {movimentacaoRecursos}
                    </tbody>
                </Table>
            ); 
    }
}