import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import UsuarioManager from '../../../../../../Services/Manager/UsuarioManager';

export default class ItemRecurso extends Component {
    constructor(props) {
        super(props);
        this.usuarioManager = new UsuarioManager();
    }

    onClickRetirar = () => {
        this.props.parent.setState({
            showModalMovimentacaoRecurso: true,
            acaoMovimentacaoRecurso: 0,
            recursoSelecionado: this.props.recurso
        });        
    }

    onClickAdicionar = () => {
        this.props.parent.setState({
            showModalMovimentacaoRecurso: true,
            acaoMovimentacaoRecurso: 1,
            recursoSelecionado: this.props.recurso
        });
    }

    onclickUpdate = () => {
        this.props.parent.setState({
            recursoSelecionado: this.props.recurso,
            doRedirectEdit: true
        });
    }

    onClickExcluir = () => {
        this.props.parent.setState({
            showModalExcluirRecurso: true,
            recursoSelecionado: this.props.recurso
        });
    }

    render() {
        return(
            <tr>                        
                <td>{this.props.recurso.descricao}</td>
                <td>{this.props.recurso.observacao ? this.props.recurso.observacao : '-'}</td>
                <td>{this.props.recurso.quantidade}</td>
                <td>
                    {(this.props.recurso.quantidade > 0) &&
                        <Button bsStyle="warning" onClick={this.onClickRetirar.bind(this)}>Retirar</Button>
                    }
                    <Button style={{marginLeft: '1%'}} bsStyle="success" onClick={this.onClickAdicionar.bind(this)}>Adicionar</Button>
                    {(this.usuarioManager.getCurrentUser().role === 'Admin') && 
                        <Button style={{marginLeft: '1%'}} bsStyle="danger" onClick={this.onClickExcluir.bind(this)}>Deletar</Button>
                    }
                    <Button style={{marginLeft: '1%'}} bsStyle="primary" onClick={this.onclickUpdate.bind(this)}>Editar</Button>
                </td>
            </tr>
        );
    }
}