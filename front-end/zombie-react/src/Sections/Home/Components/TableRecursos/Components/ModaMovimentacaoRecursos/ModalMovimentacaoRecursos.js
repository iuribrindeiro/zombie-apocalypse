import React, { Component } from 'react';
import RecursosManager from '../../../../../../Services/Manager/RecursosManager';
import UsuarioManager from '../../../../../../Services/Manager/UsuarioManager';
import { Modal, Form, Col, ControlLabel, FormGroup, FormControl, Button } from 'react-bootstrap';

export default class ModalMovimentacaoRecursos extends Component {
    constructor(props) {
        super(props);
        this.recursoManager = new RecursosManager();
        this.usuarioManager = new UsuarioManager();
        this.state = {
            quantidade: false,
            submitButtonDisabled: false,
        };
    }

    handleFormSubmit = () => {
        if (this.state.quantidade) {
            this.setState({submitButtonDisabled: true});
            let homeSection = this.props.parent.props.parent;
            this.recursoManager.movimentarRecurso(
                this.props.recurso.idRecurso, this.props.acao,this.state.quantidade,
                (movimentacaoRecurso) => {
                    let novosRecursos = homeSection.state.recursos.map((recurso) => {
                        if (recurso.idRecurso === movimentacaoRecurso.recurso.idRecurso) {
                            recurso = movimentacaoRecurso.recurso;
                        }
                        return recurso;
                    });
                    let novosMovimentacaoRecursos = homeSection.state.movimentacaoRecursos;
                    novosMovimentacaoRecursos.push(movimentacaoRecurso);
                    
                    homeSection.setState({
                        recursos: novosRecursos,
                        movimentacaoRecursos: novosMovimentacaoRecursos
                    });
                    this.setState({submitButtonDisabled: false});
                    this.closeModal();
                },
                (loginErrorMessage) => {
                    homeSection.setState({loginErrorMessage: loginErrorMessage});
                },
                (error) => {
                    homeSection.setState({error: error.message}); 
                    this.setState({submitButtonDisabled: false});
                    this.closeModal();
                }
            );
        }
    }

    handleInputChange = (e) => {
        let name = e.target.name;

        if (name === 'quantidade') {
            if (this.props.acao === 0 && this.props.recurso.quantidade < (parseInt(e.target.value))) {
                return;
            }
        }

        this.setState({
            [name]: e.target.value
        });
    }

    getValidationStateQuantidade = () => {
        if (this.state.quantidade === false) {
            return '';   
        }
        const quantidade = parseInt(this.state.quantidade);
        if (!quantidade) return 'error';
        return '';
    }

    closeModal = () => {
        this.props.parent.setState({showModalMovimentacaoRecurso: false});
        this.setState({quantidade: false});
    }

    render() {
        return(
            <Modal show={this.props.mostrarModal}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.acao ? 'Adicionar' : 'Retirar'} Recurso</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <FormGroup className="row" controlId="descricao">
                            <Col sm={12}>
                                <ControlLabel>{this.props.recurso.descricao}</ControlLabel>
                            </Col>
                        </FormGroup>
                        <FormGroup className="row" controlId="quantidade" 
                            validationState={this.getValidationStateQuantidade()}>
                            <Col sm={12}>
                                <ControlLabel>Quantidade *</ControlLabel>
                                <FormControl 
                                    name="quantidade" 
                                    type="number" 
                                    placeholder="Quantidade"
                                    onChange={this.handleInputChange.bind(this)}
                                    value={this.state.quantidade}/>
                            </Col>
                        </FormGroup>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button bsStyle="success" disabled={this.state.submitButtonDisabled} onClick={this.handleFormSubmit.bind(this)}>{this.props.acao ? 'Adicionar' : 'Retirar'}</Button>
                    <Button bsStyle="danger" onClick={this.closeModal.bind(this)}>Cancelar</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}