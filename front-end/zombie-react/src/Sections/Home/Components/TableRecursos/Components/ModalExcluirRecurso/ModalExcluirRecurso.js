import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import RecursosManager from '../../../../../../Services/Manager/RecursosManager';

export default class ModalExcluirRecurso extends Component {
    constructor(props) {
        super(props);
        this.recursoManager = new RecursosManager();
        this.state = {
            buttonDisabled: false
        }
    }

    onClickExcluir = () => {
        this.setState({buttonDisabled: true});
        let homeSection = this.props.parent.props.parent;
        this.recursoManager.excluirRecurso(this.props.recurso.idRecurso, () => {
            let newRecursos = [];
            let newMovimentacaoRecursos = [];
            homeSection.state.recursos.forEach((recurso) => {
                if (recurso.idRecurso !== this.props.recurso.idRecurso) {
                    newRecursos.push(recurso);
                }
            });
            homeSection.state.movimentacaoRecursos.forEach((movimenacaoRecurso) =>{
                if (movimenacaoRecurso.recurso.idRecurso !== this.props.recurso.idRecurso) {
                    newMovimentacaoRecursos.push(movimenacaoRecurso);
                }
            });
            homeSection.setState({
                recursos: newRecursos,
                movimentacaoRecursos: newMovimentacaoRecursos
            });
            this.closeModal();
        }, (loginErrorMessage) => {
            homeSection.setState({loginErrorMessage: loginErrorMessage});
        }, (error) => {
            homeSection.setState({error: error.message}); 
            this.closeModal();
        });
    }

    closeModal = () => {
        this.props.parent.setState({showModalExcluirRecurso: false});
        this.setState({buttonDisabled: false});
    }

    render() {
        return(
            <Modal show={this.props.show}>
                <Modal.Header>
                    <Modal.Title>Deletar Recurso</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {"Deseja realmente excluir o recurso:" + this.props.recurso.descricao + "?"}
                    <h3 className="bg-danger">ATENÇÃO: TODOS OS REGISTROS DE MOVIMENTAÇÕES SERÃO EXCLUÍDOS</h3>
                </Modal.Body>
                <Modal.Footer>
                    <Button bsStyle="danger" onClick={this.onClickExcluir.bind(this)} disabled={this.state.buttonDisabled}>Excluir</Button>
                    <Button bsStyle="primary" onClick={this.closeModal.bind(this)}>Cancelar</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}