import React, { Component } from 'react';
import { Table, Button } from 'react-bootstrap';
import RecursosManager from '../../../../Services/Manager/RecursosManager';
import UsuarioManager from '../../../../Services/Manager/UsuarioManager';
import ModalMovimentacaoRecursos from './Components/ModaMovimentacaoRecursos';
import ItemRecurso from './Components/ItemRecurso';
import ModalExcluirRecurso from './Components/ModalExcluirRecurso';
import { Redirect } from 'react-router-dom';

export default class TableRecursos extends Component {
    constructor(props) {
        super(props);
        this.recursosManager = new RecursosManager();
        this.usuarioManager = new UsuarioManager();
        this.state = {
            buscandoRecursos: false,
            showModalMovimentacaoRecurso: false,
            showModalExcluirRecurso: false,
            acaoMovimentacaoRecurso: 'Adicionar',
            recursoSelecionado: {},
            doRedirectEdit: false
        };
    }

    componentWillMount = () => {
        this.setState({buscandoRecursos: true});
        this.buscarRecursos();
    }

    buscarRecursos = () => {
        this.recursosManager.findAll((recursos) => {
            this.props.parent.setState({recursos: recursos});
            this.setState({buscandoRecursos: false});
        }, (loginErrorMessage) => {
            this.props.parent.setState({loginErrorMessage: loginErrorMessage});
            this.setState({buscandoRecursos: false});
        }, (error) => {
            this.props.parent.setState({error: error.message}); 
            this.setState({buscandoRecursos: false});
        });
    }

    render() {
            if (this.state.buscandoRecursos) {
                return(
                    <div className="loader col-sm-offset-5"></div>
                );
            }

            if (this.state.doRedirectEdit) {
                let rota = "/editar-recurso/" + this.state.recursoSelecionado.idRecurso;
                return(
                    <Redirect to={rota}/>
                );
            }
                        
            const recursos = this.props.recursos.map((recurso) => 
                <ItemRecurso parent={this} recurso={recurso} key={recurso.idRecurso} />
            );

            return(
                <div>
                    <ModalMovimentacaoRecursos 
                        parent={this}
                        mostrarModal={this.state.showModalMovimentacaoRecurso} 
                        acao={this.state.acaoMovimentacaoRecurso}
                        recurso={this.state.recursoSelecionado}/>
                    <ModalExcluirRecurso 
                        parent={this}
                        show={this.state.showModalExcluirRecurso}
                        recurso={this.state.recursoSelecionado}
                    />
                    <Table responsive>
                        <thead>
                            <tr>
                                <th>Descricao</th>
                                <th>Observação</th>
                                <th>Quantidade</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            {recursos}
                        </tbody>
                    </Table>
                </div>
            ); 
    }
}