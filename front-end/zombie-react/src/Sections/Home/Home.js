import React, { Component } from 'react';
import UsuarioManager from '../../Services/Manager/UsuarioManager';
import { Redirect } from 'react-router-dom';
import TableRecursos from './Components/TableRecursos';
import TableMovimentacaoRecursos from './Components/TableMovimentacaoRecursos';
import { Alert, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'; 

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.usuarioManager = new UsuarioManager();
        this.state = {
            error: '',
            loginErrorMessage: '',
            mostrarTableMovimentacaoRecursos: this.usuarioManager.getCurrentUser() && this.usuarioManager.getCurrentUser().role === 'Admin',
            movimentacaoRecursos: [],
            recursos: [],
        };
    }

    render() {
        if (!this.usuarioManager.getCurrentUser()) {
            let rota = this.state.loginErrorMessage.length ? '/login/' + this.state.loginErrorMessage : '/login';
            return(
                <Redirect to={rota}/>
            );
        }

        return(
            <div className="container">
                {(this.state.error.length > 0) &&
                    <Alert bsStyle="danger">
                        <strong>Holy guacamole!</strong> {this.state.error}
                    </Alert> 
                }
                {(this.props.match.params.message) && 
                    <Alert bsStyle="success">
                        <strong>Holy guacamole!</strong> {this.props.match.params.message}
                    </Alert>
                }
                 <header>
                    <Button bsStyle="primary" onClick={() => {this.usuarioManager.logout(); this.forceUpdate()}}>Logout</Button>
                </header> 
                <section className="center-vertical">
                    <header>
                        <h2>Recursos</h2>
                        <Link className="btn btn-primary" to="/novo-recurso">Novo</Link>
                    </header>   
                    <div className="row col-sm-12">   
                        <TableRecursos parent={this} recursos={this.state.recursos} />
                    </div>
                    {(this.state.mostrarTableMovimentacaoRecursos) &&
                        <div>
                            <header>
                                <h2>Movimentações</h2>
                            </header>
                            <div className="row col-sm-12">
                                <TableMovimentacaoRecursos parent={this} movimentacaoRecursos={this.state.movimentacaoRecursos} />
                            </div>
                        </div>
                    }
                </section>
            </div>
        );
    }
}