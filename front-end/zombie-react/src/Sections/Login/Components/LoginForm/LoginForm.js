import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel, Button, Alert, Form } from 'react-bootstrap';
import UsuarioManager from '../../../../Services/Manager/UsuarioManager';
import { Link } from 'react-router-dom';

export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.usuarioManager = new UsuarioManager();
        this.state = {
            username: false,
            password: false
        };
    }

    handleSubmit(event) {
        event.preventDefault(); 
        if (!this.state.username.length) {
            return this.props.parent.setState({error: "Nome de usuário não pode ser vazio"});
        }

        if (!this.state.password.length) {
            return this.props.parent.setState({error: "A senha não pode ser vazia"});
        }

        this.usuarioManager.login(this.state.username, this.state.password, (usuario) => {
            this.props.parent.setState({usuarioLogado: true});
        }, (error) => {
            this.setState({error: error.message});
        });
    }

    handleChange = (e) => {
        let name = e.target.name;
        this.setState({ [name]: e.target.value });
    }

    getValidationStateUsername = () => {
        if (this.state.username === false) return '';
        const length = this.state.username.length;
        if (!length) return 'error';        
    }
    getValidationStatePassword = () => {
        if (this.state.password === false) return ''; 
        const length = this.state.password.length;
        if (!length) return 'error';
        return '';
    }

    render() {
        return(
            <div>
                <header>
                    {(this.state.error || this.props.message) &&
                        <Alert bsStyle="danger">
                            <strong>Holy guacamole!</strong> {this.state.error ? this.state.error : this.props.message ? this.props.message : ''}
                        </Alert>    
                    }
                </header>
                <Form onSubmit={this.handleSubmit.bind(this)}>
                    <FormGroup controlId="username" validationState={this.getValidationStateUsername()}>
                        <ControlLabel>Login</ControlLabel>
                        <FormControl
                            type="text"
                            value={this.state.username ? this.state.username : ''}
                            placeholder="Nome de usuário ou email"
                            onChange={this.handleChange.bind(this)}
                            name="username"
                        />
                    </FormGroup>
                    <FormGroup controlId="password" validationState={this.getValidationStatePassword()}>
                        <ControlLabel>Senha</ControlLabel>
                        <FormControl
                            type="password"
                            value={this.state.password ? this.state.password : ''}
                            onChange={this.handleChange.bind(this)}
                            name="password"
                        />
                    </FormGroup>
                    <FormGroup>
                        <Button type="submit" bsStyle="primary">
                            Entrar
                        </Button>
                        <Link style={{marginLeft: '5%'}} className="btn btn-info" to="/cadastro">Cadastrar</Link>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}