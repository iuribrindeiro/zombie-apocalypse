import React, { Component } from 'react';
import LoginForm from './Components/LoginForm';
import { Redirect } from 'react-router-dom';
import Home from '../Home';
import UsuarioManager from '../../Services/Manager/UsuarioManager'

export default class LoginSection extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.usuarioManager = new UsuarioManager();
    }

    render() {
        if (this.usuarioManager.getCurrentUser()) {
            return(
               <Redirect to="/"/>
            );
        }

        return(
            <section className="container col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                <div className="row col-sm-12 centered center-vertical">   
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h1>Entrar</h1>
                        </div>
                        <div className="panel-body">
                            <LoginForm parent={this} message={this.props.match.params.message ? this.props.match.params.message : ''} />
                        </div>
                    </div>             
                </div>
            </section>
        );
    }
}