import React, { Component } from 'react';
import { Button, FormGroup, FormControl, Form, ControlLabel, Col, Alert } from 'react-bootstrap';
import RecursosManager from '../../../../Services/Manager/RecursosManager';
import { Link } from 'react-router-dom';

export default class RecursoForm extends Component {
    constructor(props) {
        super(props);
        this.recursosManager = new RecursosManager();
    }

    handleFormSubmit = (event) => {
        event.preventDefault();

        if (this.props.recurso.descricao.length && this.props.recurso.quantidade) {
            let data = {
                observacao: this.props.recurso.observacao,
                quantidade: this.props.recurso.quantidade,
                descricao: this.props.recurso.descricao
            };

            if (this.props.id) {
                this.recursosManager.updateRecurso(this.props.id, data, 
                    this.callbackSuccessSubmit.bind(this),
                    this.callbackAuthFailedSubmit.bind(this), 
                    this.callbackErrorSubmit.bind(this));
            } else {
                this.recursosManager.salvarRecurso(data, 
                    this.callbackSuccessSubmit.bind(this), 
                    this.callbackAuthFailedSubmit.bind(this), 
                    this.callbackErrorSubmit.bind(this)
                );
            }
        }
    }

    callbackSuccessSubmit = (data) => {
        this.props.parent.setState({success: true});
    }

    callbackAuthFailedSubmit = (loginErrorMessage) => {
        this.props.parent.setState({loginErrorMessage: loginErrorMessage});
    }

    callbackErrorSubmit = (error) => {
        this.props.parent.setState({error: error.message});
    }

    getValidationStateDescricao = () => {
        if (this.props.recurso.descricao === false) {
            return '';
        } 
        const length = this.props.recurso.descricao.length;
        if (!length) return 'error';
    }
    getValidationStateQuantidade = () => {
        if (this.props.recurso.quantidade === false) {
            return '';   
        }
        const length = parseInt(this.props.recurso.quantidade);
        if (!length) return 'error';
        return '';
    }

    handleInputChange = (e) => {
        let name = e.target.name;
        this.props.parent.state.recurso[name] = e.target.value;

        this.props.parent.setState({
           recurso: this.props.parent.state.recurso
        });
    }

    componentWillMount = () => {
        if (this.props.id) {
            this.recursosManager.find(this.props.id, (recurso) => {
                this.props.parent.setState({recurso: recurso});
            }, this.callbackAuthFailedSubmit.bind(this), this.callbackErrorSubmit.bind(this));
        }
    }

    render() {
        return(
            <Form onSubmit={this.handleFormSubmit.bind(this)}>
                <FormGroup className="row" controlId="descricao" validationState={this.getValidationStateDescricao()}>
                    <Col sm={3}>
                        <ControlLabel>Descrição *</ControlLabel>
                        <FormControl 
                            onChange={this.handleInputChange.bind(this)} 
                            type="text" 
                            placeholder="Descrição"
                            name="descricao"
                            value={this.props.recurso.descricao ? this.props.recurso.descricao : ''} />
                    </Col>
                </FormGroup>
                <FormGroup className="row" controlId="quantidade" validationState={this.getValidationStateQuantidade()}>
                    <Col sm={3}>
                        <ControlLabel>Quantidade *</ControlLabel>
                        <FormControl 
                            name="quantidade" 
                            type="number" 
                            placeholder="Quantidade"
                            onChange={this.handleInputChange.bind(this)} 
                            value={this.props.recurso.quantidade ? this.props.recurso.quantidade : 0} />
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={3}>
                        <ControlLabel>Observação</ControlLabel>
                        <FormControl 
                            name="observacao"
                            componentClass="textarea" 
                            placeholder="Observação"
                            onChange={this.handleInputChange.bind(this)} 
                            value={this.props.recurso.observacao ? this.props.recurso.observacao : ''} />
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={3}>
                        <Button type="submit" bsStyle="success" >Salvar</Button>
                        <Link className="col-sm-offset-1 btn btn-warning" to="/">Cancelar</Link>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}