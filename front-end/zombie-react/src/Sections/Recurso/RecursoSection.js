import RecursosManager from '../../Services/Manager/RecursosManager';
import UsuarioManager from '../../Services/Manager/UsuarioManager';
import React, { Component } from 'react';
import RecursoForm from './Components/RecursoForm';
import { Alert } from 'react-bootstrap';

import { Redirect } from 'react-router-dom';

export default class RecursoSection extends Component {
    constructor(props) {
        super(props);
        this.usuarioManager = new UsuarioManager();
        
        this.state = {
            recurso: {
                quantidade: false,
                descricao: false,
                observacao: false,
            },
            success: false,
            error: '',
            loginErrorMessage: ''
        };
    }


    render() {

        if (!this.usuarioManager.getCurrentUser()) {
            let rota = this.state.loginErrorMessage.length ? '/login/' + this.state.loginErrorMessage : '/login';
            return(
                <Redirect to={rota}/>
            );
        } else if(this.state.success) {
            return(
                <Redirect to="/Recurso adicionado com sucesso"/>
            );
        }

        return(
            <div>
                {this.state.error.length > 0 &&
                    <Alert bsStyle="danger">
                        <strong>Holy guacamole!</strong> {this.state.error}
                    </Alert>
                }
                <div className="container col-sm-12 center-vertical">
                    <header className="">
                        <h2>{ this.props.match.params.id ? 'Editar Recurso' : 'Novo Recurso' }</h2>
                    </header>
                    <RecursoForm parent={this} recurso={this.state.recurso} id={this.props.match.params.id} />
                </div>
            </div>
        );
    }
}