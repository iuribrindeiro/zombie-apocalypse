import Requester from '../../../Requester';
import axios from 'axios';
import Querystring from 'querystring';

export default class OAuthApi {
    constructor() {
        this.requester = new Requester();
    }

    requestJWTToken = (username, password, callbackResult, callbackError) => {
        let url = 'http://localhost:52044/oauth2/token';        

        var data = Querystring.stringify({ 
            "grant_type": "password",
            "username": username,
            "password": password
        });

        axios.post(url, data).then((response) => {
            if (response.data.access_token) {
                return callbackResult(response.data);
            }

            callbackError(response)
        }).catch((error) => {
            if (error.response && error.response.status === 400) {
                error.message = 'Usuário ou senha inválidos';
            }

            callbackError(error);
        });
    }
}