import ZombieClientApi from '../ZombieClientApi';

export default class RecursosApi extends ZombieClientApi {
    constructor(keyOrToen) {
        super('recursos', 'Bearer', keyOrToen);
    }

    findMovimentacoesRecursos = (callbackResult, callbackAuthExpired, callbackError) => {
        let url = this.apiUrl + '/api/movimentacoes-recursos';
        this.requester.request(url, {}, 'get', this.headers, callbackResult, 
        (error) => {
            if (error.response && error.response.status === 401) {
                return callbackAuthExpired('Sua sessão expirou, por favor, faça login novamente');
            }
            callbackError(error);
        });
    }

    movimentarRecurso = (idRecurso, acao, quantidade, callbackResult, callbackAuthFailed, callbackError) => {
        let url = this.apiUrl + '/api/movimentacoes-recursos';
        let data = {
            idRecurso: idRecurso,
            acao: acao,
            quantidade: quantidade
        };
        this.requester.request(url, data, 'post', this.headers, callbackResult,
        (error) => {
            if (error.response && error.response.status === 401) {
                return callbackAuthFailed('Sua sessão expirou, por favor, faça login novamente');
            }
            callbackError(error);
        });
    }
}