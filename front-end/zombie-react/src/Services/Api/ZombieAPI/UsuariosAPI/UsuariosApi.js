import ZombieClientApi from '../ZombieClientApi';

export default class UsuariosApi extends ZombieClientApi {
    constructor(keyOrToken) {
        super('usuarios', 'Bearer', keyOrToken);
    }
}