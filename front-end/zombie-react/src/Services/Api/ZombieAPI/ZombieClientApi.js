import Requester from '../../Requester';

export default class ZombieClientApi {
    constructor(endpoint, tokenType, keyOrToken) {
        this.endpoint = '/api/' + endpoint;
        this.apiUrl = 'http://localhost:52044';
        this.requester = new Requester();
        this.headers = {
            'Authorization': tokenType + ' ' + keyOrToken,
            'Content-Type': 'application/json'
        }
    }

    findAll = (callbackResult, callbackAuthFailed, callbackError) => {
        let url = this.apiUrl + this.endpoint;
        this.requester.request(url, {}, 'get', this.headers, callbackResult, 
        (error) => {
            if (error.response && error.response.status === 401) {
                return callbackAuthFailed('Sua sessão expirou, por favor, faça login novamente');
            }
            callbackError(error);
        });
    }

    salvar = (data, callbackResult, callbackAuthFailed, callbackError) => {
        let url = this.apiUrl + this.endpoint;
        this.requester.request(url, data, 'post', this.headers, callbackResult,
        (error) => {
            if (error.response && error.response.status === 401) {
                return callbackAuthFailed('Sua sessão expirou, por favor, faça login novamente');
            }
            callbackError(error);
        });
    }

    excluir = (id, callbackResult, callbackAuthFailed, callbackError) => {
        let url = this.apiUrl + this.endpoint + '/' + id;
        this.requester.request(url, {}, 'delete', this.headers, callbackResult,
        (error) => {
            if (error.response && error.response.status === 401) {
                return callbackAuthFailed('Sua sessão expirou, por favor, faça login novamente');
            }
            callbackError(error);
        });
    }

    update = (id, data, callbackResult, callbackAuthFailed, callbackError) => {
        let url = this.apiUrl + this.endpoint + '/' + id;
        this.requester.request(url, data, 'put', this.headers, callbackResult,
        (error) => {
            if (error.response && error.response.status === 401) {
                return callbackAuthFailed('Sua sessão expirou, por favor, faça login novamente');
            }
            callbackError(error);
        });
    }

    find = (id, callbackResult, callbackAuthExpired, callbackError) => {
        let url = this.apiUrl + this.endpoint + '/' + id;
        this.requester.request(url, {}, 'get', this.headers, callbackResult,
        (error) => {
            if (error.response && error.response.status === 401) {
                return callbackAuthExpired('Sua sessão expirou, por favor, faça login novamente');
            }
            callbackError(error);
        });
    }
}