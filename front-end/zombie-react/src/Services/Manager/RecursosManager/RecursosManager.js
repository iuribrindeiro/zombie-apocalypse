import RecursosApi from '../../Api/ZombieAPI/RecursosAPI';
import UsuarioManager from '../UsuarioManager';

export default class RecursosManager {
    constructor() {
        this.usuarioManager = new UsuarioManager();
        this.recursosApi = this.usuarioManager.getCurrentUser() ? new RecursosApi(this.usuarioManager.getCurrentUser().JWT) : null;
    }

    find = (id, callbackResult, callbackAuthExpired, callbackError) => {
        this.recursosApi.find(id, (recursos) => {
            callbackResult(recursos);
        }, (loginErrorMessage) => {
            this.usuarioManager.logout();
            callbackAuthExpired(loginErrorMessage); 
        }, callbackError);
    }
    
    findAll = (callbackResult, callbackAuthExpired, callbackError) => {
        this.recursosApi.findAll((recursos) => {
            callbackResult(recursos);
        }, (loginErrorMessage) => {
            this.usuarioManager.logout();
            callbackAuthExpired(loginErrorMessage); 
        }, callbackError);
    }

    movimentarRecurso = (idRecurso, acao, quantidade, callbackResult, callbackAuthExpired, callbackError) => {
        this.recursosApi.movimentarRecurso(idRecurso, acao, quantidade, (movimentarRecurso) => {
            callbackResult(movimentarRecurso);
        }, (loginErrorMessage) => {
            this.usuarioManager.logout();
            callbackAuthExpired(loginErrorMessage);
        }, (error) => {
            if (error.response && error.response.status === 404) {
                error.message = 'Recurso não encontrado';
            }

            callbackError(error);
        })
    }

    salvarRecurso = (data, callbackResult, callbackAuthExpired, callbackError) => {
        this.recursosApi.salvar(data, (response) => {
            callbackResult();
        }, (loginErrorMessage) => {
            this.usuarioManager.logout();
            callbackAuthExpired(loginErrorMessage);
        },callbackError);
    }

    findMovimentacoesRecursos = (callbackResult, callbackAuthExpired, callbackError) => {        
        this.recursosApi.findMovimentacoesRecursos((recursos) => {        
            callbackResult(recursos);
        }, (loginErrorMessage) => {
            this.usuarioManager.logout();
            callbackAuthExpired(loginErrorMessage); 
        },callbackError);
    }

    excluirRecurso = (idRecurso, callbackResult, callbackAuthExpired, callbackError) => {
        this.recursosApi.excluir(idRecurso, () => {
            callbackResult();
        }, (loginErrorMessage) => {
            this.usuarioManager.logout();
            callbackAuthExpired(loginErrorMessage); 
        },callbackError);
    }

    updateRecurso = (id, data, callbackResult, callbackAuthExpired, callbackError) => {
        this.recursosApi.update(id, data, () => {
            callbackResult();
        }, (loginErrorMessage) => {
            this.usuarioManager.logout();
            callbackAuthExpired(loginErrorMessage); 
        }, callbackError);
    }
}