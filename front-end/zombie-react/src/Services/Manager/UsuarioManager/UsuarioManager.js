import OAuthApi from '../../Api/ZombieAPI/OAuthAPI';
import UsuariosApi from '../../Api/ZombieAPI/UsuariosAPI';
var jwt_decode = require('jwt-decode');

export default class UsuarioManager {
    static Admin = 'Admin';

    constructor() {
        this.oauthApi = new OAuthApi();
        this.usuariosApi = new UsuariosApi();
    }

    getCurrentUser = () => {
        let usuario = window.sessionStorage.getItem('UsuarioLogado');

        if (usuario) {
            return JSON.parse(usuario);
        }

        return null;
    }

    cadastrar = (data, callbackResult, callbackError) => {
        this.usuariosApi.salvar(data, callbackResult, () => {}, callbackError);
    }

    login = (username, password, callbackResult, callbackError) => {
        this.oauthApi.requestJWTToken(username, password, (response) => {
            let usuario = jwt_decode(response.access_token);
            usuario.JWT = response.access_token;
            window.sessionStorage.setItem('UsuarioLogado', JSON.stringify(usuario));
            callbackResult(usuario);
        }, (error) => {
            console.log(error);            
            callbackError(error);
        });
    }

    logout = () => {
        window.sessionStorage.removeItem('UsuarioLogado');
    }
}