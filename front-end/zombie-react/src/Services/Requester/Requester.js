var axios = require('axios');

export default class Requester {
    request = (url, params, method, headers, callbackResult, callbackError) => {
        let config = {
            url: url,
            headers: headers,
            method: method ? method : 'get'
        };

        if (config.method === 'get') {
            config.params = params;
        } else {
            config.data = params;
        }

        axios(config).then((response) => {
            callbackResult(response.data);
        }).catch((error) => {
            if (callbackError) {
                return callbackError(error);
            }

            console.log(error);
        });
    }   
}