﻿using System;
using System.Collections.Generic;
using ZombieAPI.Application.Servicos.Interfaces;
using ZombieAPI.Domain.Services.Interfaces;

namespace ZombieAPI.Application.Servicos
{
    public class AppServiceBase<TEntity> : IAppServiceBase<TEntity> where TEntity : class
    {
        private readonly IBaseService<TEntity> _baseService;

        public AppServiceBase(IBaseService<TEntity> baseService)
        {
            _baseService = baseService;
        }

        public void Delete(TEntity entity)
        {
            _baseService.Delete(entity);
        }

        public void Dispose()
        {
            _baseService.Dispose();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _baseService.GetAll();
        }

        public TEntity GetById(int id)
        {
            return _baseService.GetById(id);
        }

        public void Save(TEntity entity)
        {
            _baseService.Save(entity);
        }

        public void Update(TEntity entity)
        {
            _baseService.Update(entity);
        }
    }
}
