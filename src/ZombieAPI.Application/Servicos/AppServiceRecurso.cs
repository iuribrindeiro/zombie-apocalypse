﻿using System.Collections.Generic;
using ZombieAPI.Application.Servicos.Interfaces;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Entities.Enums;
using ZombieAPI.Domain.Services.Interfaces;

namespace ZombieAPI.Application.Servicos
{
    public class AppServiceRecurso : AppServiceBase<Recurso>, IAppServiceRecurso
    {
        private readonly IRecursoService _recursoService;

        public AppServiceRecurso(IRecursoService recursoService)
            : base(recursoService)
        {
            _recursoService = recursoService;
        }

        public IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso()
        {
            return _recursoService.BuscarMovimentacoesRecurso();
        }

        public IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso(Usuario usuario)
        {
            return _recursoService.BuscarMovimentacoesRecurso(usuario);
        }

        public MovimentacaoRecurso MovimentarRecurso(Usuario usuario, Recurso recurso, int quantidade, Acao acao)
        {
            return _recursoService.MovimentarRecurso(usuario, recurso, quantidade, acao);
        }
    }
}
