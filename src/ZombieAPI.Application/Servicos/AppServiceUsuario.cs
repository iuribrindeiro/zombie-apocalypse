﻿
using System.Collections.Generic;
using ZombieAPI.Application.Servicos.Interfaces;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Services.Interfaces;

namespace ZombieAPI.Application.Servicos
{
    public class AppServiceUsuario : IAppServiceUsuario
    {
        private readonly IUsuarioService _usuarioService;

        public AppServiceUsuario(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        public void Delete(Usuario entity)
        {
            _usuarioService.Delete(entity);
        }

        public void Dispose()
        {
            _usuarioService.Dispose();
        }

        public Usuario findByUserName(string username)
        {
            return _usuarioService.findByUserName(username);
        }

        public IEnumerable<Usuario> GetAll()
        {
            return _usuarioService.GetAll();
        }

        public Usuario GetById(string id)
        {
            return _usuarioService.GetById(id);
        }

        public void Save(Usuario entity)
        {
            _usuarioService.Save(entity);
        }

        public void Update(Usuario entity)
        {
            _usuarioService.Update(entity);
        }
    }
}
