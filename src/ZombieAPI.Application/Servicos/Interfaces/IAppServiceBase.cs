﻿using System;
using System.Collections.Generic;

namespace ZombieAPI.Application.Servicos.Interfaces
{
    public interface IAppServiceBase<TEntity> : IDisposable where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        void Delete(TEntity entity);

        void Save(TEntity entity);

        void Update(TEntity entity);
    }
}
