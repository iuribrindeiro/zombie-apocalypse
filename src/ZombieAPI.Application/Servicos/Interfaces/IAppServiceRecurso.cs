﻿using System.Collections.Generic;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Entities.Enums;

namespace ZombieAPI.Application.Servicos.Interfaces
{
    public interface IAppServiceRecurso : IAppServiceBase<Recurso>
    {
        MovimentacaoRecurso MovimentarRecurso(Usuario usuario, Recurso recurso, int quantidade, Acao acao);

        IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso();

        IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso(Usuario usuario);
    }
}
