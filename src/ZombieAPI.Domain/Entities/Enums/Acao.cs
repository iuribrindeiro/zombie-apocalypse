﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAPI.Domain.Entities.Enums
{
    public enum Acao
    {
        Saida,
        Entrada,
        Criacao
    }
}
