﻿using System;
using ZombieAPI.Domain.Entities.Enums;

namespace ZombieAPI.Domain.Entities
{
    public class MovimentacaoRecurso
    {
        public int Id { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual Recurso Recurso { get; set; }

        public Acao Acao { get; set; }

        public int Quantidade { get; set; }

        public DateTime DataAtualizacao { get; set; }
    }
}
