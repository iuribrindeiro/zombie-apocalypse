﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombieAPI.Domain.Entities
{
    public class Recurso
    {
        public int IdRecurso { get; set; }

        public string Descricao { get; set; }

        public int Quantidade { get; set; }

        public string Observacao { get; set; }

        public virtual ICollection<MovimentacaoRecurso> MovimentosRecursos { get; set; }
    }
}
