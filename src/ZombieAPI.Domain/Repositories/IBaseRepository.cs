﻿using System;
using System.Collections.Generic;

namespace ZombieAPI.Domain.Repositories
{
    public interface IBaseRepository<TEntity> : IDisposable
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        void Delete(TEntity entity);

        void Save(TEntity entity);

        void Update(TEntity entity);
    }
}
