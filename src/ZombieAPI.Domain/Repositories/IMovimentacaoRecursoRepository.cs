﻿using System.Collections.Generic;
using ZombieAPI.Domain.Entities;

namespace ZombieAPI.Domain.Repositories
{
    public interface IMovimentacaoRecursoRepository : IBaseRepository<MovimentacaoRecurso>
    {
        IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso(Usuario usuario);
    }
}
