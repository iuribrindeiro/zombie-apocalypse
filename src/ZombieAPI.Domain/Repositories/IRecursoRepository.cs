﻿using System.Collections.Generic;
using ZombieAPI.Domain.Entities;

namespace ZombieAPI.Domain.Repositories
{
    public interface IRecursoRepository : IBaseRepository<Recurso>
    {

    }
}
