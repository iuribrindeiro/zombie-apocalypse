﻿using System;
using System.Collections.Generic;
using ZombieAPI.Domain.Entities;

namespace ZombieAPI.Domain.Repositories
{
    public interface IUsuarioRepository : IDisposable
    {
        IEnumerable<Usuario> GetAll();

        Usuario GetById(string id);

        void Delete(Usuario entity);

        void Save(Usuario entity);

        void Update(Usuario entity);

        Usuario findByUserName(string username);
    }
}
