﻿using System;
using System.Collections.Generic;
using ZombieAPI.Domain.Repositories;
using ZombieAPI.Domain.Services.Interfaces;

namespace ZombieAPI.Domain.Services
{
    public class BaseService<TEntity> : IDisposable, IBaseService<TEntity> where TEntity : class
    {
        private readonly IBaseRepository<TEntity> _baseRepository;

        public BaseService(IBaseRepository<TEntity> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public void Delete(TEntity entity)
        {
            _baseRepository.Delete(entity);
        }

        public void Dispose()
        {
            _baseRepository.Dispose();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _baseRepository.GetAll();
        }

        public TEntity GetById(int id)
        {
            return _baseRepository.GetById(id);
        }

        public void Save(TEntity entity)
        {
            _baseRepository.Save(entity);
        }

        public void Update(TEntity entity)
        {
            _baseRepository.Update(entity);
        }
    }
}
