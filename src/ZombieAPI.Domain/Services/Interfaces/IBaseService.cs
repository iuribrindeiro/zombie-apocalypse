﻿using System;
using System.Collections.Generic;

namespace ZombieAPI.Domain.Services.Interfaces
{
    public interface IBaseService<TEntity> : IDisposable where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        void Delete(TEntity entity);

        void Save(TEntity entity);

        void Update(TEntity entity);
    }
}
