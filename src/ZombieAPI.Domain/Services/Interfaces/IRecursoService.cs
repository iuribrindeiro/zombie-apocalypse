﻿using System.Collections.Generic;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Entities.Enums;

namespace ZombieAPI.Domain.Services.Interfaces
{
    public interface IRecursoService : IBaseService<Recurso>
    {
        MovimentacaoRecurso MovimentarRecurso(Usuario usuario, Recurso recurso, int quantidade, Acao acao);

        IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso();

        IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso(Usuario usuario);
    }
}
