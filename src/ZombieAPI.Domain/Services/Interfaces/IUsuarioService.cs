﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZombieAPI.Domain.Entities;

namespace ZombieAPI.Domain.Services.Interfaces
{
    public interface IUsuarioService : IDisposable
    {
        IEnumerable<Usuario> GetAll();

        Usuario GetById(string id);

        void Delete(Usuario entity);

        void Save(Usuario entity);

        void Update(Usuario entity);

        Usuario findByUserName(string username);
    }
}
