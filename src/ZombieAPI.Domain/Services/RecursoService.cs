﻿using System.Collections.Generic;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Entities.Enums;
using ZombieAPI.Domain.Repositories;
using ZombieAPI.Domain.Services.Interfaces;

namespace ZombieAPI.Domain.Services
{
    public class RecursoService : BaseService<Recurso>, IRecursoService
    {
        private readonly IRecursoRepository _recursoRepository;

        private readonly IMovimentacaoRecursoRepository _movimentacaoRecursoRepository;

        public RecursoService(IRecursoRepository recursoRepository, IMovimentacaoRecursoRepository movimentacaoRecursoRepository)
            : base(recursoRepository)
        {
            _recursoRepository = recursoRepository;
            _movimentacaoRecursoRepository = movimentacaoRecursoRepository;
        }

        public IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso()
        {
            return _movimentacaoRecursoRepository.GetAll();
        }

        public IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso(Usuario usuario)
        {
            return _movimentacaoRecursoRepository.BuscarMovimentacoesRecurso(usuario);
        }

        public MovimentacaoRecurso MovimentarRecurso(Usuario usuario, Recurso recurso, int quantidade, Acao acao)
        {
            MovimentacaoRecurso movimentacaoRecurso = new MovimentacaoRecurso()
            {
                Usuario = usuario,
                Recurso = recurso,
                Acao = acao,
                Quantidade = quantidade
            };

            if (acao == Acao.Entrada || acao == Acao.Saida)
            {
                recurso.Quantidade = (acao == Acao.Entrada) ? recurso.Quantidade + quantidade : recurso.Quantidade - quantidade;
                _recursoRepository.Update(recurso);
            }

            _movimentacaoRecursoRepository.Save(movimentacaoRecurso);

            return movimentacaoRecurso;
        }
    }
}
