﻿using System.Collections.Generic;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Repositories;
using ZombieAPI.Domain.Services.Interfaces;

namespace ZombieAPI.Domain.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public void Delete(Usuario entity)
        {
            _usuarioRepository.Delete(entity);
        }

        public void Dispose()
        {
            _usuarioRepository.Dispose();
        }

        public Usuario findByUserName(string username)
        {
            return _usuarioRepository.findByUserName(username);
        }

        public IEnumerable<Usuario> GetAll()
        {
            return _usuarioRepository.GetAll();
        }

        public Usuario GetById(string id)
        {
            return _usuarioRepository.GetById(id);
        }

        public void Save(Usuario entity)
        {
            _usuarioRepository.Save(entity);
        }

        public void Update(Usuario entity)
        {
            _usuarioRepository.Update(entity);
        }
    }
}
