﻿using System.ComponentModel.DataAnnotations;


namespace ZombieAPI.Infra.CrossCutting.Identity.Model
{
    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }
}
