﻿using System.ComponentModel.DataAnnotations;

namespace ZombieAPI.Infra.CrossCutting.Identity.Model
{
    class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }
}
