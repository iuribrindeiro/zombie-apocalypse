﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using ZombieAPI.Application.Servicos;
using ZombieAPI.Application.Servicos.Interfaces;
using ZombieAPI.Domain.Repositories;
using ZombieAPI.Domain.Services;
using ZombieAPI.Domain.Services.Interfaces;
using ZombieAPI.Infra.CrossCutting.Identity.Configuration;
using ZombieAPI.Infra.CrossCutting.Identity.Context;
using ZombieAPI.Infra.CrossCutting.Identity.Model;
using ZombieAPI.Infra.Data.Context;
using ZombieAPI.Infra.Data.Repositories;
using ZombieAPI.Infra.Data.Repository;

namespace ZombieAPI.Infra.CrossCutting.IoC
{
    public class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            //clico de vida por request
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            //contexts do Infra.Identity e Infra.Data
            container.Register<ZombieAPIContext>(Lifestyle.Scoped);
            container.Register<ApplicationDbContext>(Lifestyle.Scoped);   

            //Manager relacionados com Identity
            //OBS: O ApplicationUser é registrado dessa maneira por ter 2 construtores
            container.Register<IUserStore<ApplicationUser>>(
                () => new UserStore<ApplicationUser>(new ApplicationDbContext()),
                Lifestyle.Scoped
            );
            container.Register<IRoleStore<IdentityRole, string>>(
                () => new RoleStore<IdentityRole>(), 
                Lifestyle.Scoped
            );
            container.Register<ApplicationRoleManager>(Lifestyle.Scoped);
            container.Register<ApplicationUserManager>(Lifestyle.Scoped);            
            container.Register<ApplicationSignInManager>(Lifestyle.Scoped);
            container.Register<ISecureDataFormat<AuthenticationTicket>>(
                () => new FakeTicket(), 
                Lifestyle.Scoped
            );
            container.Register<UserManager<ApplicationUser>>(Lifestyle.Scoped);
            //FIM - Manager relacionados com Identity

            //Repositories
            container.Register(typeof(IBaseRepository<>), typeof(BaseRepository<>), Lifestyle.Scoped);
            container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Scoped);
            container.Register<IRecursoRepository, RecursoRepository>(Lifestyle.Scoped);
            container.Register<IMovimentacaoRecursoRepository, MovimentacaoRecursoRepository>(Lifestyle.Scoped);

            //Services
            container.Register(typeof(IBaseService<>), typeof(BaseService<>), Lifestyle.Scoped);
            container.Register<IRecursoService, RecursoService>(Lifestyle.Scoped);
            container.Register<IUsuarioService, UsuarioService>(Lifestyle.Scoped);

            //App Services
            container.Register(typeof(IAppServiceBase<>), typeof(AppServiceBase<>), Lifestyle.Scoped);
            container.Register<IAppServiceRecurso, AppServiceRecurso>(Lifestyle.Scoped);
            container.Register<IAppServiceUsuario, AppServiceUsuario>(Lifestyle.Scoped);
        }
    }
}
