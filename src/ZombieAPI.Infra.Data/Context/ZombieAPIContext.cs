﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Infra.Data.EntityConfig;

namespace ZombieAPI.Infra.Data.Context
{
    public class ZombieAPIContext : DbContext
    {
        public const string CONNECTIONSTRING = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\iurib\source\repos\ZombiesAPI\zombie-apocalypse\src\ZombieAPI.Infra.Data\zombies-test.mdf;Integrated Security=True";

        public ZombieAPIContext()
            : base(CONNECTIONSTRING)
        {
            
        }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Recurso> Recursos { get; set; }

        public DbSet<MovimentacaoRecurso> MovimentacoesRecursos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UsuarioConfig());
            modelBuilder.Configurations.Add(new RecursoConfig());
            modelBuilder.Configurations.Add(new MovimentacaoRecursoConfig());

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }
    }
}