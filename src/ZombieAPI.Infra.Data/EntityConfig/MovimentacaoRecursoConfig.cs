﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ZombieAPI.Domain.Entities;

namespace ZombieAPI.Infra.Data.EntityConfig
{
    public class MovimentacaoRecursoConfig : EntityTypeConfiguration<MovimentacaoRecurso>
    {
        public MovimentacaoRecursoConfig()
        {
            HasKey(mr => mr.Id);

            Property(mr => mr.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            Property(mr => mr.Acao)
                .IsRequired();

            Property(mr => mr.Quantidade)
                .IsRequired();

            Property(mr => mr.DataAtualizacao)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            HasRequired(mr => mr.Recurso)
                .WithMany(r => r.MovimentosRecursos);

            HasRequired(mr => mr.Usuario)
                .WithMany(u => u.MovimentacoesRecursos);

            ToTable("MovimentacaoRecursos");
        }
    }
}
