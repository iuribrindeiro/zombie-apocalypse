﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ZombieAPI.Domain.Entities;

namespace ZombieAPI.Infra.Data.EntityConfig
{
    public class RecursoConfig : EntityTypeConfiguration<Recurso>
    {
        public RecursoConfig()
        {
            HasKey(r => r.IdRecurso);

            Property(r => r.IdRecurso)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(r => r.IdRecurso)
                .IsRequired();

            Property(r => r.Observacao)
                .IsOptional();

            Property(r => r.Quantidade)
                .IsRequired();

            ToTable("Recursos");
        }
    }
}
