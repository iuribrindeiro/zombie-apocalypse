namespace ZombieAPI.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Recursos",
                c => new
                {
                    IdRecurso = c.Int(nullable: false, identity: true),
                    Descricao = c.String(),
                    Quantidade = c.Int(nullable: false),
                    Observacao = c.String(),
                })
                .PrimaryKey(t => t.IdRecurso);

            CreateTable(
                "dbo.MovimentacaoRecursos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Acao = c.Int(nullable: false),
                        Quantidade = c.Int(nullable: false),
                        DataAtualizacao = c.DateTime(nullable: false),
                        Recurso_IdRecurso = c.Int(nullable: false),
                        Usuario_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recursos", t => t.Recurso_IdRecurso, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.Usuario_Id, cascadeDelete: true)
                .Index(t => t.Recurso_IdRecurso)
                .Index(t => t.Usuario_Id);          
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MovimentacaoRecursos", "Usuario_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.MovimentacaoRecursos", "Recurso_IdRecurso", "dbo.Recursos");
            DropIndex("dbo.MovimentacaoRecursos", new[] { "Usuario_Id" });
            DropIndex("dbo.MovimentacaoRecursos", new[] { "Recurso_IdRecurso" });
            DropTable("dbo.Recursos");
            DropTable("dbo.MovimentacaoRecursos");
        }
    }
}
