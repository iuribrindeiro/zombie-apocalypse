﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ZombieAPI.Domain.Repositories;
using ZombieAPI.Infra.Data.Context;

namespace ZombieAPI.Infra.Data.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly ZombieAPIContext _db;

        public BaseRepository(ZombieAPIContext db)
        {
            _db = db;
        }

        public void Delete(TEntity entity)
        {
            _db.Set<TEntity>().Remove(entity);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _db.Set<TEntity>().ToList();
        }

        public TEntity GetById(int id)
        {
            return _db.Set<TEntity>().Find(id);
        }

        public void Save(TEntity entity)
        {
            _db.Set<TEntity>().Add(entity);
            _db.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            _db.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Modified;
            _db.SaveChanges();
        }
    }
}
