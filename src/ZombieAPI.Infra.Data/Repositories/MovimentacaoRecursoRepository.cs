﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Repositories;
using ZombieAPI.Infra.Data.Context;

namespace ZombieAPI.Infra.Data.Repositories
{
    public class MovimentacaoRecursoRepository : BaseRepository<MovimentacaoRecurso>, IMovimentacaoRecursoRepository
    {
        private readonly ZombieAPIContext _db;

        public MovimentacaoRecursoRepository(ZombieAPIContext db)
            : base(db)
        {
            _db = db;
        }

        public IEnumerable<MovimentacaoRecurso> BuscarMovimentacoesRecurso(Usuario usuario)
        {
            return _db.MovimentacoesRecursos.Where(mr => mr.Usuario == usuario);
        }
    }
}
