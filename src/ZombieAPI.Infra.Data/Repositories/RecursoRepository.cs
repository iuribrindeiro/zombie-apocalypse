﻿using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Repositories;
using ZombieAPI.Infra.Data.Context;

namespace ZombieAPI.Infra.Data.Repositories
{
    public class RecursoRepository : BaseRepository<Recurso>, IRecursoRepository
    {
        private readonly ZombieAPIContext _db;

        public RecursoRepository(ZombieAPIContext db)
            : base(db)
        {
            _db = db;
        }
    }
}
