﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Repositories;
using ZombieAPI.Infra.Data.Context;

namespace ZombieAPI.Infra.Data.Repository
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly ZombieAPIContext _db;

        public UsuarioRepository(ZombieAPIContext db)
        {
            _db = db;
        }

        public void Delete(Usuario entity)
        {
            _db.Usuarios.Remove(entity);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public Usuario findByUserName(string username)
        {
            return _db.Usuarios.Where(u => u.UserName == username).First();
        }

        public IEnumerable<Usuario> GetAll()
        {
            return _db.Usuarios.ToList();
        }

        public Usuario GetById(string id)
        {
            return _db.Usuarios.Find(id);
        }

        public void Save(Usuario entity)
        {
            _db.Usuarios.Add(entity);
            _db.SaveChanges();
        }

        public void Update(Usuario entity)
        {
            _db.Entry<Usuario>(entity).State = EntityState.Modified;
            _db.SaveChanges();
        }
    }
}