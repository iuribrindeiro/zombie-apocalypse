﻿using System.Web;
using System.Web.Http;
using ZombieAPI.Infra.CrossCutting.IoC;
using Microsoft.Owin;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.WebApi;

namespace ZombieAPI.Presentation.API
{
    public static class DependencyInjector
    {
        public static void Register(HttpConfiguration config)
        {
            Container container = new Container();
            
#pragma warning disable CS0618 // 'WebApiRequestLifestyle' is obsolete: 'WebApiRequestLifestyle has been deprecated. Please use SimpleInjector.Lifestyles.AsyncScopedLifestyle instead.'
            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
#pragma warning restore CS0618 // 'WebApiRequestLifestyle' is obsolete: 'WebApiRequestLifestyle has been deprecated. Please use SimpleInjector.Lifestyles.AsyncScopedLifestyle instead.'

            // Chamada dos módulos do Simple Injector
            BootStrapper.RegisterServices(container);

            // Necessário para registrar o ambiente do Owin que é dependência do Identity
            // Feito fora da camada de IoC para não levar o System.Web para fora
            container.Register(() =>
            {
                if (HttpContext.Current != null && HttpContext.Current.Items["owin.Environment"] == null && container.IsVerifying())
                {
                    return new OwinContext().Authentication;
                }
                return HttpContext.Current.GetOwinContext().Authentication;

            }, Lifestyle.Scoped);

            // This is an extension method from the integration package.
            container.RegisterWebApiControllers(config);

            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}