﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZombieAPI.Application.Servicos.Interfaces;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Entities.Enums;
using ZombieAPI.Presentation.API.Filters;
using ZombieAPI.Presentation.API.Models;

namespace ZombieAPI.Presentation.API.Controllers
{
    [RoutePrefix("api/movimentacoes-recursos")]
    [Authorize]
    public class MovimentacaoRecursosController : ApiController
    {
        private readonly IAppServiceRecurso _appServiceRecurso;

        private readonly IAppServiceUsuario _appServiceUsuario;

        public MovimentacaoRecursosController(IAppServiceRecurso appServiceRecurso, IAppServiceUsuario appServiceUsuario)
        {
            _appServiceRecurso = appServiceRecurso;
            _appServiceUsuario = appServiceUsuario;
        }

        [Route("")]
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IEnumerable<MovimentacaoRecurso> getMovimentacaoRecursos()
        {
            return _appServiceRecurso.BuscarMovimentacoesRecurso();
        }

        [ValidateModel]
        [HttpPost]
        [Route("")]
        public object MovimentarRecurso(MovimentacaoRecursoViewModel movimentacaoRecursoViewModel)
        {
            Recurso recurso = _appServiceRecurso.GetById(movimentacaoRecursoViewModel.IdRecurso);

            if (recurso != null)
            {
                if (movimentacaoRecursoViewModel.Acao == Acao.Saida && 
                    recurso.Quantidade < movimentacaoRecursoViewModel.Quantidade)
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }

                Usuario usuario = _appServiceUsuario.findByUserName(User.Identity.Name);

                MovimentacaoRecurso movimentacaoRecurso = _appServiceRecurso.MovimentarRecurso(usuario, recurso, movimentacaoRecursoViewModel.Quantidade, movimentacaoRecursoViewModel.Acao);

                movimentacaoRecursoViewModel.Usuario = movimentacaoRecurso.Usuario;
                movimentacaoRecursoViewModel.DataAtualizacao = movimentacaoRecurso.DataAtualizacao;
                movimentacaoRecursoViewModel.Recurso = movimentacaoRecurso.Recurso;

                return movimentacaoRecursoViewModel;
            }

            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
    }
}
