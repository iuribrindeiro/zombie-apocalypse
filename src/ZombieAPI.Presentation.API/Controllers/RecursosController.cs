﻿using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZombieAPI.Application.Servicos.Interfaces;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Entities.Enums;
using ZombieAPI.Presentation.API.Filters;
using ZombieAPI.Presentation.API.Models;

namespace ZombieAPI.Presentation.API.Controllers
{
    [Authorize]
    public class RecursosController : ApiController
    {
        private readonly IAppServiceRecurso _appServiceRecurso;

        private readonly IAppServiceUsuario _appServiceUsuario;

        public RecursosController(IAppServiceRecurso appServiceRecurso, IAppServiceUsuario appServiceUsuario)
        {
            _appServiceRecurso = appServiceRecurso;
            _appServiceUsuario = appServiceUsuario;
        }

        // GET: api/Recursos
        public IEnumerable<RecursoViewModel> Get()
        {            
            IEnumerable<Recurso> recursos = _appServiceRecurso.GetAll();
            List<RecursoViewModel> recursosModel = new List<RecursoViewModel>();

            foreach(Recurso recurso in recursos)
            {
                recursosModel.Add(new RecursoViewModel()
                {
                    IdRecurso = recurso.IdRecurso,
                    Descricao = recurso.Descricao,
                    Observacao = recurso.Observacao,
                    Quantidade = recurso.Quantidade
                });
            }

            return recursosModel;
        }

        // GET: api/Recursos/5
        public object Get(int id)
        {
            Recurso recurso = _appServiceRecurso.GetById(id);

            if (recurso != null)
            {
                return new RecursoViewModel()
                {
                    IdRecurso = recurso.IdRecurso,
                    Descricao = recurso.Descricao,
                    Observacao = recurso.Observacao,
                    Quantidade = recurso.Quantidade
                };
            }

            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        [ValidateModel]
        public HttpResponseMessage Post(RecursoViewModel recurso)
        {
            try
            {
                var objRecurso = new Recurso() {
                    IdRecurso = recurso.IdRecurso,
                    Descricao = recurso.Descricao,
                    Observacao = recurso.Observacao,
                    Quantidade = recurso.Quantidade 
                };
                _appServiceRecurso.Save(objRecurso);

                Usuario usuario = _appServiceUsuario.findByUserName(User.Identity.Name);
                _appServiceRecurso.MovimentarRecurso(usuario, objRecurso, objRecurso.Quantidade, Acao.Criacao);
                return new HttpResponseMessage(HttpStatusCode.Created);
            } catch(DbEntityValidationException e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [ValidateModel]
        public HttpResponseMessage Put(int id, RecursoViewModel recurso)
        {
            try
            {
                Recurso oldRecurso = _appServiceRecurso.GetById(id);
                oldRecurso.Descricao = recurso.Descricao;
                oldRecurso.Observacao = recurso.Observacao;
                oldRecurso.Quantidade = recurso.Quantidade;

                _appServiceRecurso.Update(oldRecurso);
                return new HttpResponseMessage(HttpStatusCode.OK);
            } catch
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                Recurso recurso = _appServiceRecurso.GetById(id);

                if (recurso != null)
                {
                    _appServiceRecurso.Delete(recurso);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }

                return new HttpResponseMessage(HttpStatusCode.NotFound);
            } catch
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }
}
