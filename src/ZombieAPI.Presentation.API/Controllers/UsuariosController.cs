﻿using Microsoft.AspNet.Identity;
using System.Web.Http;
using ZombieAPI.Application.Servicos.Interfaces;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Infra.CrossCutting.Identity.Model;
using ZombieAPI.Presentation.API.Filters;
using ZombieAPI.Presentation.API.Models;

namespace ZombieAPI.Presentation.API.Controllers
{
    [AllowAnonymous]
    public class UsuariosController : ApiController
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UsuariosController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        [ValidateModel]
        public UsuarioViewModel Post(UsuarioViewModel usuarioViewModel)
        {
            var user = new ApplicationUser {
                UserName = usuarioViewModel.Email,
                Email = usuarioViewModel.Email,
            };

            _userManager.Create(user, usuarioViewModel.Password);
            _userManager.AddToRole(user.Id, "Membro");

            return usuarioViewModel;
        }
    }
}
