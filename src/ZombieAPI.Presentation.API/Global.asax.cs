﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using ZombieAPI.Presentation.API.App_Start;

namespace ZombieAPI.Presentation.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(DependencyInjector.Register);
            GlobalConfiguration.Configure(FilterConfig.Configure);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.End();
            }
        }
}
}
