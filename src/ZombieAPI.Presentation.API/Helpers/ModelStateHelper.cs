﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;

namespace ZombieAPI.Presentation.API.Helpers
{
    public static class ModelStateHelper
    {
        public static IEnumerable Errors(ModelStateDictionary modelState)
        {
            Dictionary<string, string> r = new Dictionary<string, string>();
            foreach (var ms in modelState)
            {
                if (ms.Value.Errors.Count() >= 1)
                {
                    r.Add(ms.Key, ms.Value.Errors.First().ErrorMessage);
                }
            }

            return r;
        }
    }
}