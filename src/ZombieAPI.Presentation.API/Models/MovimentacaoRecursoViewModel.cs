﻿using System;
using System.ComponentModel.DataAnnotations;
using ZombieAPI.Domain.Entities;
using ZombieAPI.Domain.Entities.Enums;

namespace ZombieAPI.Presentation.API.Models
{
    public class MovimentacaoRecursoViewModel
    {
        [Required]
        public Acao Acao { get; set; }

        [Required]
        public int IdRecurso { get; set; }

        [Required]
        public int Quantidade { get; set; }

        public Usuario Usuario { get; set; }

        public Recurso Recurso { get; set; }

        public DateTime DataAtualizacao { get; set; }
    }
}