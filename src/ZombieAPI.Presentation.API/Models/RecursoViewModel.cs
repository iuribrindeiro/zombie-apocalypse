﻿using System.ComponentModel.DataAnnotations;

namespace ZombieAPI.Presentation.API.Models
{
    public class RecursoViewModel
    {
        public virtual int IdRecurso { get; set; }

        [Required(ErrorMessage = "{0} não pode ser vázio")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "{0} não pode ser vazio")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "Somente valores inteiros")]
        [Range(0, int.MaxValue, ErrorMessage = "Por favor, informe um valor maior que {1}")]
        public int Quantidade { get; set; }
        
        public string Observacao { get; set; }
    }
}