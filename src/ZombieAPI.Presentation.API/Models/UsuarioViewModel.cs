﻿
using System.ComponentModel.DataAnnotations;

namespace ZombieAPI.Presentation.API.Models
{
    public class UsuarioViewModel
    {
        [Required]
        [MinLength(5)]
        public string Nome { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [RegularExpression("^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,16}$", 
            ErrorMessage = "A senha deve conter pelo menos uma letra maiúscula, um caractere especial e um número")]
        public string Password { get; set; }
    }
}