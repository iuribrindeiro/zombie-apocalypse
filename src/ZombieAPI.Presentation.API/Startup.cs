﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup( typeof(ZombieAPI.Presentation.API.Startup))]

namespace ZombieAPI.Presentation.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
        }
    }
}